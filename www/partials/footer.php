<footer data-scroll-section class="c-footer || u-text-center">
    <div class="o-container -small" id="contact">
        <h3 class="c-heading -h2" data-scroll data-scroll-speed="1">
            <span class="u-text-gradient">
                Become irreplaceable to your customers.
            </span>
        </h3>
    </div>
    <div class="c-footer_rail">
        <a href="mailto:hi@minami.design" target="_blank" rel="noopener" class="c-footer_rail_email">hi@minami.design</a>

        <div class="c-rail" data-scroll data-scroll-repeat data-scroll-speed="2">
            <div class="c-rail_wrap">
                <?php for($x=1; $x<=2; $x++): ?>
                <div class="c-rail_item">
                    <div class="c-rail_label">
                        Say Hello Say Hello
                    </div>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
    
    <div class="o-container -small">
        <div class="c-footer_social">
            <ul class="c-footer_social_list">
                <li class="c-footer_social_item">
                    <a href="https://dribbble.com/minami" target="_blank" rel="noopener" class="c-footer_social_link">
                        <span class="c-footer_social_label">Dribbble</span>
                        <span class="c-footer_social_icon">
                            <svg class="c-footer_social_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                            <svg class="c-footer_social_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                        </span>
                    </a>
                </li>
                <li class="c-footer_social_item">
                    <a href="https://www.instagram.com/minami_digital/" target="_blank" rel="noopener" class="c-footer_social_link">
                        <span class="c-footer_social_label">Instagram</span>
                        <span class="c-footer_social_icon">
                            <svg class="c-footer_social_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                            <svg class="c-footer_social_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                        </span>
                    </a>
                </li>
                <li class="c-footer_social_item">
                    <a href="https://www.linkedin.com/company/minamidigital/" target="_blank" rel="noopener" class="c-footer_social_link">
                        <span class="c-footer_social_label">LinkedIn</span>
                        <span class="c-footer_social_icon">
                            <svg class="c-footer_social_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                            <svg class="c-footer_social_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                        </span>
                    </a>
                </li>
                <li class="c-footer_social_item">
                    <a href="https://medium.com/@minami_digital" target="_blank" rel="noopener" class="c-footer_social_link">
                        <span class="c-footer_social_label">Medium</span>
                        <span class="c-footer_social_icon">
                            <svg class="c-footer_social_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                            <svg class="c-footer_social_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>