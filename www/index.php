<?php include 'partials/header.php'; ?>
    <div class="o-scroll" data-module-scroll="main">
        <main>
            <div data-scroll-section>
                <div class="c-header-mask" id="header-page" data-module-hero>

                    <div class="c-header-mask_pannel -left" data-scroll data-scroll-speed="7" data-scroll-direction="horizontal" data-scroll-target="#header-page" data-scroll-position="top">
                        <div class="c-header-mask_bg"></div>
                    </div>
                    <div class="c-header-mask_pannel -right" data-scroll data-scroll-speed="-7" data-scroll-direction="horizontal" data-scroll-target="#header-page" data-scroll-position="top">
                        <div class="c-header-mask_bg"></div>
                    </div>

                    <div class="c-header-mask_opacity" id="header-opacity"></div>
                    <div class="c-header-mask_opacity_bg" data-scroll data-scroll-target="#header-opacity" data-scroll-id="headerOpacity" data-scroll-call="headerOpacity" data-scroll-repeat data-scroll-position="top"></div>

                    <section class="c-section || c-hero" id="hero">
                        <div class="o-container -medium" data-scroll data-scroll-speed="-1" data-scroll-targert="#hero" data-scroll-position="top">
                            <h1 class="c-heading -h1 || c-hero_title">
                                <div class="u-anim">
                                    <div class="u-text-gradient">
                                        We Use Design to Make You Win
                                    </div>
                                </div>
                            </h1>
                            <p class="c-hero_text || u-anim -delay-2">
                                <span class="c-hero_text_hide">Use the</span> 
                                <button class="c-hero_text_button" data-hero="button">Power of Design</button> 
                                <span class="c-hero_text_hide">to stand out</span>
                            </p>
                        </div>

                        <div href="#block-1" data-scroll-to class="c-hero_scroll">
                            <div class="c-hero_scroll_line"></div>
                        </div>

                        <div class="c-carousel-hero || c-carousel">
                            <div class="c-carousel-hero_container || swiper-container" data-hero="carousel">
                                <div class="c-carousel_list || c-carousel-hero_list || swiper-wrapper">
                                    <div class="c-carousel_item || c-carousel-hero_item || swiper-slide" data-carousel-hero="item">
                                        <div data-swiper-parallax-opacity="0" data-swiper-parallax-duration="600" data-swiper-parallax-scale="0.8">
                                            <div class="c-carousel-hero_number">(01)</div>
                                            <h2 class="c-heading -h2 -no-margin" data-swiper-parallax-duration="600" data-swiper-parallax="100">
                                                <span class="u-text-gradient">
                                                    Design builds meaning <br>for your audiences
                                                </span>
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="c-carousel_item || c-carousel-hero_item || swiper-slide" data-hero="item">
                                        <div data-swiper-parallax-opacity="0" data-swiper-parallax-duration="600" data-swiper-parallax-scale="0.8">
                                            <div class="c-carousel-hero_number">(02)</div>
                                            <h2 class="c-heading -h2 -no-margin" data-swiper-parallax-duration="600" data-swiper-parallax="100">
                                                <span class="u-text-gradient">
                                                    Design gives your people <br>a purpose and a mission
                                                </span>
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="c-carousel_item || c-carousel-hero_item || swiper-slide" data-hero="item">
                                        <div data-swiper-parallax-opacity="0" data-swiper-parallax-duration="600" data-swiper-parallax-scale="0.8">
                                            <div class="c-carousel-hero_number">(03)</div>
                                            <h2 class="c-heading -h2 -no-margin" data-swiper-parallax-duration="600" data-swiper-parallax="100">
                                                <span class="u-text-gradient">
                                                    Design makes something <br>that never existed before  
                                                </span>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="c-section" id="block-1">
                        <div class="o-container">
                            <div class="o-layout -gutter -flex@from-medium -middle">
                                <div class="o-layout_item u-1/2@from-small">
                                    <div data-scroll data-scroll-speed="1" data-scroll-target="#block-1">
                                        <div class="c-section_content -padding-top">
                                            <h2 class="c-heading -h2" data-scroll data-scroll-speed="1">
                                                <span class="u-text-gradient">
                                                    There are too many lookalikes out there. 
                                                </span>
                                            </h2>
                                            <div class="u-anim-scroll" data-scroll>
                                                <h3 class="c-heading -h3 || u-anim-scroll_element -delay-1">
                                                    Sameness is not good for business. Don't fall flat. Your clients deserve the unique and best version of your company.
                                                </h3>
                                                <div class="u-anim-scroll_element -delay-2 || u-margin-bottom@to-small">
                                                    <?php include 'partials/cta-contact.php'; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="o-layout_item u-1/2@from-small">
                                    <figure class="c-figure -parallax -offset-right || u-anim-img" id="img-block-1" data-scroll>
                                        <div class="c-figure_wrap">
                                            <div class="c-figure_bg" data-scroll data-scroll-speed="-2" data-scroll-target="#img-block-1" alt="" style="background-image:url(assets/images/img-01.jpg);"></div>
                                            <img class="c-figure_img" alt="" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 602 702'%3E%3C/svg%3E">
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="c-section -fullheight || u-text-center" id="block-2">
                        <div class="o-container -small">
                            <h2 class="c-heading -h2" data-scroll data-scroll-speed="1">
                                <span class="u-text-gradient">
                                    We know how hard it is <br>to win in a crowded marketplace
                                </span>
                            </h2>
                            <div class="u-anim-scroll" data-scroll>
                                <a href="#contact" data-scroll-to class="c-button -line || u-anim-scroll_element">
                                    <span class="c-button_label">Talk to a design advisor</span>
                                    <span class="c-button_icon">
                                        <svg class="c-button_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                        <svg class="c-button_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                    </span>
                                </a>
                            </a>
                        </div>
                    </section>

                    <section class="c-section -fullheight || u-text-center" id="block-3">
                        <div class="o-container -small">
                            <h2 class="c-heading -h2" data-scroll data-scroll-speed="1">
                                <span class="u-text-gradient">
                                    That's why we have the right design plan to cut throught the noise
                                </span>
                            </h2>
                            <div class="u-anim-scroll" data-scroll>
                                <a href="#contact" data-scroll-to class="c-button -line || u-anim-scroll_element">
                                    <span class="c-button_label">Talk to a design advisor</span>
                                    <span class="c-button_icon">
                                        <svg class="c-button_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                        <svg class="c-button_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </section>
                    
                    <section class="c-section || u-overflow" id="block-4">
                        <div class="o-container">
                            <div class="o-layout -reverse@to-medium">
                                <div class="o-layout_item u-1/2@from-medium">
                                    <h2 class="c-heading -h2" data-scroll data-scroll-speed="1">
                                        <span class="u-text-gradient">
                                            We can help you make 2020 a better year.
                                        </span>
                                    </h2>
                                </div>
                                <div class="o-layout_item u-1/2@from-medium || u-text-right">
                                    <div class="c-badge" data-scroll data-scroll-speed="2" data-scroll-target="#block-4" data-scroll-repeat data-scroll-delay="0.05">
                                        <div class="c-badge_text">
                                            <img alt="" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 400 400'%3E%3C/svg%3E" data-load-src="assets/images/badge-text.svg">
                                        </div>
                                        <div class="c-badge_year">
                                            <img alt="" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 400 400'%3E%3C/svg%3E" data-load-src="assets/images/badge.svg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="c-carousel" data-module-carousel-about>
                            <div class="o-container">
                                <div class="u-relative">
                                    <div class="c-carousel_control || c-caousel-about_control || u-anim-scroll" data-scroll>
                                        <div class="u-anim-scroll_element">
                                            <button class="c-carousel_control_button -prev" data-carousel-about="prev" type="button">
                                                <svg class="c-carousel_control_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                                <svg class="c-carousel_control_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                            </button>
                                        </div>
                                        <div class="u-anim-scroll_element -delay-1">
                                            <button class="c-carousel_control_button -next" data-carousel-about="next" type="button">
                                                <svg class="c-carousel_control_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                                <svg class="c-carousel_control_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="o-layout -gutter -flex -reverse@to-medium">
                                        <div class="o-layout_item u-1/3@from-medium -flex -space-between">
                                            <div class="u-anim-scroll" data-scroll>
                                                <div class="u-anim-scroll_element">
                                                    <h2 class="c-caousel-about_title || c-heading -h3">Chat one-on-one with a Design specialist to see what we can do for your business.</h2>
                                                </div>
                                                <div class="u-anim-scroll_element">
                                                    <?php include 'partials/cta-contact.php'; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="o-layout_item u-2/3@from-medium">
                                            <div class="c-carousel_container">
                                                <div class="c-carousel-about_container || swiper-container || js-hover-drag" data-carousel-about="carousel">
                                                    <div class="c-carousel_list || swiper-wrapper">
                                                        <div class="c-carousel_item || swiper-slide" data-carousel-about="item">
                                                            <div class="c-carousel-about_item">
                                                                <div class="c-carousel-about_rail">
                                                                    <div class="c-carousel-about_rail_wrap">
                                                                        <?php for($x=1; $x<=2; $x++): ?>
                                                                        <div class="c-carousel-about_rail_item">
                                                                            <div class="c-carousel-about_rail_label">North America</div>
                                                                            <div class="c-carousel-about_rail_label">Europe</div>
                                                                            <div class="c-carousel-about_rail_label">South America</div>
                                                                            <div class="c-carousel-about_rail_label">Asia</div>
                                                                        </div>
                                                                        <?php endfor; ?>
                                                                    </div>
                                                                </div>
                                                                <div class="c-carousel-about_info">
                                                                    <div class="c-carousel-about_info_title">10</div>
                                                                    <div class="c-carousel-about_info_number">(01)</div>
                                                                </div>
                                                                <h3 class="c-heading -h3 -no-margin || c-carousel-about_text">Helped 10 companies in 4 continents become design leaders.</h3>
                                                            </div>
                                                        </div>
                                                        <div class="c-carousel_item || swiper-slide" data-carousel-about="item">
                                                            <div class="c-carousel-about_item">
                                                                <div class="c-carousel-about_bg" style="background-image: url(assets/images/carousel-about-2.png)"></div>
                                                                <div class="c-carousel-about_info">
                                                                    <div class="c-carousel-about_info_title">01</div>
                                                                    <div class="c-carousel-about_info_number">(02)</div>
                                                                </div>
                                                                <h3 class="c-heading -h3 -no-margin || c-carousel-about_text">We made 1 Y-Combinator finalist</h3>
                                                            </div>
                                                        </div>
                                                        <div class="c-carousel_item || swiper-slide" data-carousel-about="item">
                                                            <div class="c-carousel-about_item">
                                                                <div class="c-carousel-about_bg" style="background-image: url(assets/images/carousel-about-1.jpg)"></div>
                                                                <div class="c-carousel-about_info">
                                                                    <div class="c-carousel-about_info_title">500</div>
                                                                    <div class="c-carousel-about_info_number">(03)</div>
                                                                </div>
                                                                <h3 class="c-heading -h3 -no-margin || c-carousel-about_text">Our clients secured over $500M in investments</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="c-section" id="block-family">
                        <div class="c-carousel || c-carousel-family" data-module-carousel-family>
                            <div class="o-container">
                                <div class="u-anim-scroll" data-scroll>
                                    <div class="c-carousel_label || u-anim-scroll_element">Our services</div>
                                </div>
                            </div>
                            <div class="c-carousel_container || u-overflow-visible">
                                <div class="c-carousel-family_container || swiper-container || js-hover-drag" data-carousel-family="carousel">
                                    <div class="c-carousel_list || swiper-wrapper">
                                        <div class="c-carousel_item || c-carousel-family_item || swiper-slide" data-carousel-family="item">
                                            <div class="o-container">
                                                <div class="o-layout">
                                                    <div class="o-layout_item u-8/12@from-medium">
                                                        <div class="c-carousel-family_content">
                                                            <div data-scroll data-scroll-speed="1" data-scroll-target="#block-family">
                                                                <h2 class="c-carousel-family_title" data-swiper-parallax="300" data-swiper-parallax-duration="900">
                                                                    <span class="u-text-gradient -soft u-inline">
                                                                        Strategic 
                                                                    </span>
                                                                    <sup class="c-carousel-family_title_sup">(01)</sup><br>
                                                                    <span class="u-text-gradient u-inline">
                                                                        Design 
                                                                    </span>
                                                                </h2>
                                                            </div>
                                                            <div class="u-anim-scroll" data-scroll>
                                                                <div class="c-section_content -padding-right || u-anim-scroll_element">
                                                                    <h3 class="c-heading -h3">
                                                                        Our Strategic Design Masterplan helps companies redefine how problems are approached, and to identify new opportunities for improvement and growth.
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="o-layout_item u-4/12@from-medium">
                                                        <div class="c-carousel-family_video">
                                                            <div class="o-ratio u-263:345 || c-carousel-family_video_inner" data-swiper-parallax-opacity="0" data-swiper-parallax-duration="900" data-swiper-parallax="1000" data-swiper-parallax-scale="0.5">
                                                                <video class="o-ratio_content || c-carousel-family_player" preload="auto" autoplay muted playsinline loop data-module-video="0">
                                                                    <source src="assets/videos/service-1.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="c-carousel_item || c-carousel-family_item || swiper-slide" data-carousel-family="item">
                                            <div class="o-container">
                                                <div class="o-layout">
                                                    <div class="o-layout_item u-8/12@from-medium">
                                                        <div class="c-carousel-family_content">
                                                            <div data-scroll data-scroll-speed="1" data-scroll-target="#block-family">
                                                                <h2 class="c-carousel-family_title" data-swiper-parallax="300" data-swiper-parallax-duration="900">
                                                                    <span class="u-text-gradient -soft u-inline">
                                                                        Product 
                                                                    </span>
                                                                    <sup class="c-carousel-family_title_sup">(02)</sup><br>
                                                                    <span class="u-text-gradient u-inline">
                                                                        Design 
                                                                    </span>
                                                                </h2>
                                                            </div>
                                                            <div class="u-anim-scroll" data-scroll>
                                                                <div class="c-section_content -padding-right || u-anim-scroll_element">
                                                                    <h3 class="c-heading -h3">
                                                                        Our Digital Product Design Masterplan helps Startups create digital products and mobile apps that play a meaningful role in people's lives.
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="o-layout_item u-4/12@from-medium">
                                                        <div class="c-carousel-family_video">
                                                            <div class="o-ratio u-263:345 || c-carousel-family_video_inner" data-swiper-parallax-opacity="0" data-swiper-parallax-duration="900" data-swiper-parallax="1000" data-swiper-parallax-scale="0.5">
                                                                <video class="o-ratio_content || c-carousel-family_player" preload="auto" muted playsinline loop data-module-video="1">
                                                                    <source src="assets/videos/service-2.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="c-carousel_item || c-carousel-family_item || swiper-slide" data-carousel-family="item">
                                            <div class="o-container">
                                                <div class="o-layout -gutter-small">
                                                    <div class="o-layout_item u-8/12@from-medium">
                                                        <div class="c-carousel-family_content">
                                                            <div data-scroll data-scroll-speed="1" data-scroll-target="#block-family">
                                                                <h2 class="c-carousel-family_title" data-swiper-parallax="300" data-swiper-parallax-duration="900">
                                                                    <span class="u-text-gradient -soft u-inline">
                                                                        User 
                                                                    </span>
                                                                    <sup class="c-carousel-family_title_sup">(03)</sup><br>
                                                                    <span class="u-text-gradient u-inline">
                                                                        Experience 
                                                                    </span>
                                                                </h2>
                                                            </div>
                                                            <div class="u-anim-scroll" data-scroll>
                                                                <div class="c-section_content -padding-right || u-anim-scroll_element">
                                                                    <h3 class="c-heading -h3">
                                                                        Our User Experience Strategy Masterplan helps tech-based companies create user experiences that elevate their products and match the speed of the world.
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="o-layout_item u-4/12@from-medium">
                                                        <div class="c-carousel-family_video">
                                                            <div class="o-ratio u-263:345 || c-carousel-family_video_inner" data-swiper-parallax-opacity="0" data-swiper-parallax-duration="900" data-swiper-parallax="1000" data-swiper-parallax-scale="0.5">
                                                                <video class="o-ratio_content || c-carousel-family_player" preload="auto" muted playsinline loop data-module-video="2">
                                                                    <source src="assets/videos/service-3.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="c-carousel_item || c-carousel-family_item || swiper-slide" data-carousel-family="item">
                                            <div class="o-container">
                                                <div class="o-layout -gutter-small">
                                                    <div class="o-layout_item u-8/12@from-medium">
                                                        <div class="c-carousel-family_content">
                                                            <div data-scroll data-scroll-speed="1" data-scroll-target="#block-family">
                                                                <h2 class="c-carousel-family_title" data-swiper-parallax="300" data-swiper-parallax-duration="900">
                                                                    <span class="u-text-gradient -soft u-inline">
                                                                        Service
                                                                    </span>
                                                                    <sup class="c-carousel-family_title_sup">(04)</sup><br>
                                                                    <span class="u-text-gradient -soft u-inline">
                                                                        Innovation
                                                                    </span>
                                                                </h2>
                                                            </div>
                                                            <div class="u-anim-scroll" data-scroll>
                                                                <div class="c-section_content -padding-right || u-anim-scroll_element">
                                                                    <h3 class="c-heading -h3">
                                                                        Our Service Innovation Masterplan helps brands create service experiences that make their customers and internal teams feel well taken care of.
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="o-layout_item u-4/12@from-medium">
                                                        <div class="c-carousel-family_video">
                                                            <div class="o-ratio u-263:345 || c-carousel-family_video_inner" data-swiper-parallax-opacity="0" data-swiper-parallax-duration="900" data-swiper-parallax="1000" data-swiper-parallax-scale="0.5">
                                                                <video class="o-ratio_content || c-carousel-family_player" preload="auto" muted playsinline loop data-module-video="3">
                                                                    <source src="assets/videos/service-4.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="c-carousel-family_control">
                                <div class="o-container">
                                    <div class="u-anim-scroll" data-scroll>
                                        <div class="c-carousel_control-reverse">
                                            <div class="c-section_content -padding-bottom@from-medium -padding-top@to-medium || u-anim-scroll_element">
                                                <?php include 'partials/cta-contact.php'; ?>
                                            </div>
                                            <div class="c-carousel_control">
                                                <div class="u-anim-scroll_element -delay-1">
                                                    <button class="c-carousel_control_button -prev || u-pointer-auto" data-carousel-family="prev" type="button">
                                                        <svg class="c-carousel_control_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                                        <svg class="c-carousel_control_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                                    </button>
                                                </div>
                                                <div class="u-anim-scroll_element -delay-2">
                                                    <button class="c-carousel_control_button -next || u-pointer-auto" data-carousel-family="next" type="button">
                                                        <svg class="c-carousel_control_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                                        <svg class="c-carousel_control_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </section>

                </div>
            </div>

            <div data-scroll-section>
                <section class="c-section -no-padding-top -black">
                    <div class="o-container">
                        <div class="c-video || js-hover-video" data-module-video-toggler data-video-toggler="toggler" data-video-toggler-host="vimeo" data-video-toggler-id="449044639">
                            <div class="c-video_icon">
                                <div class="c-video_label">Play</div>
                            </div>
                            <figure class="c-figure -parallax -overlay || u-anim-img" id="img-video" data-scroll>
                                <div class="c-figure_wrap">
                                    <video class="o-ratio_content || c-video_video" preload="auto" muted playsinline loop autoplay>
                                        <source src="assets/videos/ReelPreviewLoop-compress.mp4" type="video/mp4">
                                    </video>
                                    <img class="c-figure_img" data-scroll data-scroll-speed="-1" data-scroll-target="#img-video" alt="" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1360 768'%3E%3C/svg%3E">
                                </div>
                            </figure>
                        </div>
                    </div>
                </section>
            </div>

            <div data-scroll-section>
               <section class="c-section -black" id="block-how">
                    <div class="c-carousel || c-carousel-how" data-module-carousel-how>
                        <div class="o-container">
                            <div class="c-carousel-how_content -padding-left || u-anim-scroll" data-scroll>
                                <div class="c-carousel_label || u-anim-scroll_element">How to start</div> 
                            </div>
                        </div>
                        <div class="c-carousel_container || c-carousel-how_container || swiper-container || js-hover-drag" data-carousel-how="carousel">
                            <div class="c-carousel_list || swiper-wrapper">
                                <div class="c-carousel_item || c-carousel-how_item || swiper-slide" data-carousel-how="item">
                                    <div class="o-container">
                                        <div class="c-carousel-how_content -padding-left -padding-right">
                                            <div class="c-heading_sup || c-carousel-how_number" data-swiper-parallax-opacity="0" data-swiper-parallax-duration="900" data-swiper-parallax="1000">(01)</div>
                                            <div data-scroll data-scroll-speed="1" data-scroll-target="#block-how">
                                                <h2 class="c-heading -h2 || c-carousel-how_title" data-swiper-parallax="520" data-swiper-parallax-duration="900">
                                                    <span class="u-text-gradient u-inline">
                                                        We review your <strong class="u-text-gradient_bold">challenges</strong>
                                                    </span>
                                                </h2>
                                            </div>
                                            <div class="c-carousel-how_video" data-swiper-parallax-opacity="0" data-swiper-parallax-duration="900" data-swiper-parallax="150">
                                                <video preload="auto" autoplay muted playsinline loop data-module-video="how-0">
                                                    <source src="assets/videos/Video1.mp4" type="video/mp4">
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="c-carousel_item || c-carousel-how_item || swiper-slide" data-carousel-how="item">
                                    <div class="o-container">
                                        <div class="c-carousel-how_content -padding-left -padding-right">
                                            <div class="c-heading_sup || c-carousel-how_number" data-swiper-parallax-opacity="0" data-swiper-parallax-duration="900" data-swiper-parallax="1000">(02)</div>
                                            <div data-scroll data-scroll-speed="1" data-scroll-target="#block-how">
                                                <h2 class="c-heading -h2 || c-carousel-how_title" data-swiper-parallax="520" data-swiper-parallax-duration="900">
                                                    <span class="u-text-gradient u-inline">
                                                        We create the right <strong class="u-text-gradient_bold">design plan</strong>
                                                    </span>
                                                </h2>
                                            </div>
                                            <div class="c-carousel-how_video" data-swiper-parallax-opacity="0" data-swiper-parallax-duration="900" data-swiper-parallax="150">
                                                <video preload="auto" muted playsinline loop data-module-video="how-1">
                                                    <source src="assets/videos/getfitted.mp4" type="video/mp4">
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="c-carousel_item || c-carousel-how_item || swiper-slide" data-carousel-how="item">
                                    <div class="o-container">
                                        <div class="c-carousel-how_content -padding-left -padding-right">
                                            <div class="c-heading_sup || c-carousel-how_number" data-swiper-parallax-opacity="0" data-swiper-parallax-duration="900" data-swiper-parallax="1000">(03)</div>
                                            <div data-scroll data-scroll-speed="1" data-scroll-target="#block-how">
                                                <h2 class="c-heading -h2 || c-carousel-how_title" data-swiper-parallax="520" data-swiper-parallax-duration="900">
                                                    <span class="u-text-gradient u-inline">
                                                        We transform your <strong class="u-text-gradient_bold">business</strong>
                                                    </span>
                                                </h2>
                                            </div>
                                            <div class="c-carousel-how_video" data-swiper-parallax-opacity="0" data-swiper-parallax-duration="900" data-swiper-parallax="150">
                                                <video preload="auto" muted playsinline loop data-module-video="how-2">
                                                    <source src="assets/videos/Video3.mp4" type="video/mp4">
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="c-carousel-how_control || u-pointer-none">
                            <div class="o-container">
                                <div class="c-carousel-how_content -padding-left || c-carousel_control-reverse || u-anim-scroll" data-scroll>
                                    <div class="c-section_content -padding-bottom@from-medium -padding-top@to-medium || u-anim-scroll_element">
                                        <?php include 'partials/cta-contact.php'; ?>
                                    </div>
                                    <div class="c-carousel_control">
                                        <div class="u-anim-scroll_element -delay-1">
                                            <button class="c-carousel_control_button -prev || u-pointer-auto" data-carousel-how="prev" type="button">
                                                <svg class="c-carousel_control_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                                <svg class="c-carousel_control_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                            </button>
                                        </div>
                                        <div class="u-anim-scroll_element -delay-2">
                                            <button class="c-carousel_control_button -next || u-pointer-auto" data-carousel-how="next" type="button">
                                                <svg class="c-carousel_control_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                                <svg class="c-carousel_control_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               </section>
            </div>

            <div data-scroll-section>
               <section class="c-section -black -fullheight || u-text-center" id="block-checklist">
                    <div class="o-container -small">
                        <div class="c-block-checklist">
                            <h2 class="c-heading -h2" data-scroll data-scroll-speed="1" data-scroll-target="#block-checklist">
                                <span class="u-text-gradient">
                                    Take a quiz and see how Design can transform your business
                                </span>
                            </h2>
                            <div class="u-anim-scroll" data-scroll>
                                <a href="https://minamidesign.typeform.com/to/WBV8fYfG" class="c-button -line || u-anim-scroll_element" target="_blank" rel="noopener">
                                    <span class="c-button_label">Take the quiz (3-5 minutes)</span>
                                    <span class="c-button_icon">
                                        <svg class="c-button_arrow -normal" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                        <svg class="c-button_arrow -hover" role="img"><use xlink:href="assets/images/sprite.svg#arrow-right"></use></svg>
                                    </span>
                                </a>
                            </div>
                            <div class="c-block-checklist_img" id="block-checklist-img" data-scroll>
                                <div class="c-block-checklist_img_bg" style="background-image: url(assets/images/pdf.jpg)" data-scroll data-scroll-speed="-1" data-scroll-target="#block-checklist-img"></div>
                            </div>
                        </div>
                    </div>
               </section>
            </div>

            <div data-scroll-section>
               <section class="c-section -black -fullheight || u-text-center" id="text-blur">
                    <div class="o-container" data-scroll data-scroll-speed="2" data-scroll-target="#text-blur">
                        <h2 class="c-heading -h2 || c-text-blur" data-scroll data-scroll-repeat>
                            <span class="u-text-gradient">
                                Don't get <span><span class="c-text-blur_item">lost</span> in the <span class="c-text-blur_item -delay">noise.</span>
                            </span>
                        </h2>
                    </div>
               </section>
            </div>
        </main>
        <?php include 'partials/footer.php'; ?>
    </div>
<?php include 'partials/footer.end.php'; ?>