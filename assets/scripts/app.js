import modular from 'modujs';
import * as modules from './modules';
import globals from './globals';
import { html } from './utils/environment';

const app = new modular({
    modules: modules
});

window.onload = (event) => {
    const $style = document.getElementById("stylesheet");

    let isMobile = /Android|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
    window.isMobile = isMobile;

    if(window.isMobile) {
        html.classList.add('is-mobile');
    }

    // Browser detection
    // ==========================================================================
    const browserParser = bowser.getParser(window.navigator.userAgent);
    const browser = browserParser.getBrowser()

    if(browser.name === "Internet Explorer") {
        html.classList.add('is-ie')
    } else if(browser.name === "Firefox") {
        html.classList.add('is-firefox')
    }

    if ($style.isLoaded) {
        init();
    } else {
        $style.addEventListener('load', (event) => {
            init();
        });
    }
};

function init() {
    window.lottiePromises = []
    
    app.init(app);
    globals();

    setTimeout(() => {
        html.classList.remove('is-loading');
        html.classList.add('is-loaded', 'is-ready');
    }, 2300);
}

