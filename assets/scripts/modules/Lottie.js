import { APP_NAME } from '../utils/environment';
import { module } from 'modujs';

const OPTIONS_DEFAULT = {
    loop: true,
    autoplay: true,
    animType: 'svg'
}

export default class extends module {
    constructor(m) {
        super(m);

        this.container = this.el
        this.fileName = this.getData('file');
        this.path = this.getData('path')
        this.autoplay = typeof this.getData('autoplay') === "string"
    }

    init() {
        // fix mask white?
        // lottie.setLocationHref(document.location.href);
        // if(!this.autoplay) {
        //     this.mouseenterBind = this.play.bind(this)
        //     this.el.addEventListener('mouseenter', this.mouseenterBind)

        //     this.mouseleaveBind = this.pause.bind(this)
        //     this.el.addEventListener('mouseleave', this.mouseleaveBind)
        // }

        this.lottiePromise = this.loadAnimationByName(this.fileName).then(data => {
            if(this.toDestroy) return

            this.lottieAnim = lottie.loadAnimation({
                container: this.container,
                animType: OPTIONS_DEFAULT.animType,
                loop: OPTIONS_DEFAULT.loop,
                autoplay: OPTIONS_DEFAULT.autoplay,
                animationData: data,
                
                //rendererSettings: {
                //    preserveAspectRatio: 'none'
                //}
            })

            this.animation = {
                lottieAnim: this.lottieAnim,
                data
            }

            let lottiePromise = new Promise((resolve, reject) => {
                this.lottieAnim.addEventListener('DOMLoaded', () => {
                    resolve()

                    if(!this.autoplay) this.lottieAnim.pause()

                    window.lottiePromises.splice(window.lottiePromises.indexOf(this.lottiePromise), 1)
                })
            })

            return lottiePromise
        }).catch((err) => {
            console.error(err)
        });

        // requieres window.lottiePromises = [] in app.js
        window.lottiePromises.push(this.lottiePromise)
    }

    /**
     * This function fetches (in a promise) the data of the given animation (found by it's name)
     * By doing so, it also re-maps the assets URLs to match the good folder
     *
     * @param {string} name - The filename of the animation to load (with extension)
     * @returns {Promise}
     */
    loadAnimationByName(name) {
        return new Promise((resolve, reject) => {
            fetch(this.path+name+'.json') // get the json
             .then((resp) => resp.json()) // parse it
             .then(data => {
                // re-map all assets URLs to match the good folder
                data.assets.map(item => {
                    if(item.u && item.u.length) {
                        item.u = this.path+item.u;
                    }
                    return item;
                })

                // return the data
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            })
        })
    }

    play() {
        this.lottieAnim.play()
    }

    pause() {
        setTimeout(() => {
            this.lottieAnim.pause()
        }, 800)
    }

    destroy() {
        super.destroy();

        // if(!this.autoplay) {
        //     this.el.removeEventListener('mouseenter', this.mouseenterBind)
        //     this.el.removeEventListener('mouseleave', this.mouseenterBind)
        // }

        this.toDestroy = true

        cancelAnimationFrame(this.raf)

        let actualDestroy = () => {
            if(this.animation && this.animation.lottieAnim && this.animation.lottieAnim.destroy)
                this.animation.lottieAnim.destroy()
            this.animation.lottieAnim = null
        }
    }
}
