import { APP_NAME } from '../utils/environment';
import { module } from 'modujs';

export default class extends module {
    constructor(m) {
        super(m);
        
    }

    init() {
       this.video = this.el
    }


    play() {
        this.video.play()
    }

    pause() {
        setTimeout(() => {
            this.el.pause()
        }, 400)
    }

    destroy() {
        super.destroy();
    }
}
