import { module } from 'modujs';
import lottieLight from 'lottie-web/build/player/lottie_light';

export default class extends module {
    constructor(m) {
        super(m);
    }

    init() {
        const intro = lottieLight.loadAnimation({
            container: this.el,
            renderer: 'svg',
            loop: false,
            autoplay: true,
            path: 'assets/motions/loader.json',
            rendererSettings: {
                preserveAspectRatio: 'xMinYMin slice',
            }
        });

        intro.setSpeed(2);
    }
}
