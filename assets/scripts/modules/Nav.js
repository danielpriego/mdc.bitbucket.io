import { module } from 'modujs';


export default class extends module {
    constructor(m) {
        super(m);
        
        this.events = {
            click: {
                link: 'onClick'
            }
        }
    }

    init() {
        
    }

    onClick(e) {
        const target = e.curTarget.getAttribute('href');
        this.call('scrollTo', {target: target}, 'Scroll', 'main');
    }

    destroy() {
    }
}
