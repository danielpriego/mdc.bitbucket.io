import { module } from 'modujs';
import { html } from '../utils/environment'

const CLASS = {
    SLIDERACTIVE: `has-hero-slider-active`,
}

export default class extends module {
    constructor(m) {
        super(m);

        this.events = {
            mouseenter: {
                button: 'heroSliderShow',
            },
            mouseleave: {
                button: 'heroSliderHide'
            },
        }

        this.carousel = this.$('carousel')[0]
        if(!this.carousel) this.carousel = this.el

        this.arrowNext = this.$('next')[0]
        this.arrowPrev = this.$('prev')[0]
    }

    init() {
        this.length = this.$('item').length

        const args = {
            initialSlide: 0,
            speed: 600,
            loop: true,
            spaceBetween: 0,
            grabCursor: true,
            slidesPerView: 1,
            threshold: 3,
            parallax: true,
            loopedSlides: 2,
            direction: 'vertical',
            autoplay: {
                delay: 1800,
                disableOnInteraction: true,
            },
            navigation: {
                nextEl: this.arrowNext,
                prevEl: this.arrowPrev,
            },
        }

        if(this.length > 1) this.carousel = new Swiper(this.carousel, args)
        this.carousel.autoplay.stop();
    }

    heroSliderShow() {
        this.carousel.autoplay.start();
        html.classList.add(CLASS.SLIDERACTIVE)
    }

    heroSliderHide() {
        this.carousel.autoplay.stop();
        html.classList.remove(CLASS.SLIDERACTIVE)
    }

    destroy() {
        if(this.carousel && this.carousel.destroy) this.carousel.destroy(true, true)
    }
}
