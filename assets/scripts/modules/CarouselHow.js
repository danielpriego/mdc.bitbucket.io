import { module } from 'modujs';


export default class extends module {
    constructor(m) {
        super(m);
        this.carousel = this.$('carousel')[0]
        if(!this.carousel) this.carousel = this.el

        this.arrowNext = this.$('next')[0]
        this.arrowPrev = this.$('prev')[0]
    }

    init() {
        this.length = this.$('item').length

        const args = {
            speed: 900,
            loop: false,
            spaceBetween: 0,
            grabCursor: true,
            slidesPerView: 1,
            threshold: 1,
            parallax: true,
            navigation: {
                nextEl: this.arrowNext,
                prevEl: this.arrowPrev,
            },
        }

        if(this.length > 1) {
            this.carousel = new Swiper(this.carousel, args)

            this.index = 0
            this.carousel.on('slideChangeTransitionStart', () => {
                this.call('pause', null, 'Video', `how-${this.index}`)
                this.index = this.carousel.activeIndex
                this.call('play', null, 'Video', `how-${this.index}`)
            });

            this.carousel.on('transitionEnd', () => {
                this.index = this.carousel.activeIndex
                this.call('play', null, 'Video', `how-${this.index}`)
            });
        }
    }

    destroy() {
        if(this.carousel && this.carousel.destroy) this.carousel.destroy(true, true)
    }
}
