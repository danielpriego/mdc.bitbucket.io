import { module } from 'modujs';
import LocomotiveScroll from 'locomotive-scroll';

export default class extends module {
    constructor(m) {
        super(m);
    }

    init() {
        setTimeout(() => {
            this.scroll = new LocomotiveScroll({
                el: this.el,
                smooth: true
            });

            this.scroll.on('call', (func, way, obj) => {
                if (func === 'headerOpacity') {
                    if (way === 'exit') {
                        obj.el.style.opacity = 1;
                    }
                } else {
                    this.call(func[0],{way,obj},func[1],func[2]);
                }
            });

            this.scroll.on('scroll', (instance) => {
                const headerOpacity = instance.currentElements['headerOpacity'];

                if (headerOpacity) {
                    const opacity = (headerOpacity.progress);
                    headerOpacity.el.style.opacity = opacity;
                } 
            })
        }, 400)
    }

    toggleLazy(args) {
        let src = this.getData('lazy', args.obj.el)
        if(src.length) {
            if(args.obj.el.tagName == "IMG") {
                args.obj.el.src = src
            } else {
                args.obj.el.style.backgroundImage = `url(${src})`
            }
            this.setData('lazy', '', args.obj.el)
        }
    }

    update() {
        if(this.scroll && this.scroll.update)
            this.scroll.update()
    }

    // scrollTo(options) {
    //     this.scroll.scrollTo(options.target, -70);
    // }

    scrollTo(params) {
        if(this.scroll && this.scroll.scrollTo) this.scroll.scrollTo(params.target, params.options);
    }

    destroy() {
        this.scroll.destroy();
    }
}
