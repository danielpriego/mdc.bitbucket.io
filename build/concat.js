import gulp from 'gulp';
import gulpConcat from 'gulp-concat';
import paths from '../mconfig.json';

function concat() {
    return gulp
        .src([
            `${paths.scripts.vendors.src}*.js`,
            `node_modules/swiper/js/swiper.min.js`,
            'node_modules/lottie-web/build/player/lottie.min.js',
            `node_modules/bowser/es5.js`,
            `node_modules/gsap/dist/gsap.min.js`
        ])
        .pipe(gulpConcat(`${paths.scripts.vendors.main}.js`))
        .pipe(gulp.dest(paths.scripts.dest));
}

export default concat;
